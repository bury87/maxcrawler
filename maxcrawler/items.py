# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class DebenhamsItem(scrapy.Item):
    id = scrapy.Field()
    url = scrapy.Field()
    category = scrapy.Field()
    brand = scrapy.Field()
    img = scrapy.Field()
    name = scrapy.Field()
    price = scrapy.Field()
    pass

class TescoItem(scrapy.Item):
    id = scrapy.Field()
    url = scrapy.Field()
    category = scrapy.Field()
    img = scrapy.Field()
    name = scrapy.Field()
    price = scrapy.Field()
    pass