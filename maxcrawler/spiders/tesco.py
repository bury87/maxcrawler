import scrapy

from maxcrawler.items import TescoItem


class TescoSpider(scrapy.Spider):
    name = "tesco"

    def start_requests(self):
        url = 'https://www.tesco.ie/groceries/'
        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        urls = response.xpath(".//ul[@class='navigation Groceries']/li/a/@href").extract()
        for url in urls:
            if '/sale' not in url:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse1)

    def parse1(self, response):
        urls = response.xpath(".//div[@class='footer']//li/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse2)

    def parse2(self, response):
        next_page = response.xpath(".//p[@class='next']/a/@href").extract_first()
        if next_page is not None:
            yield scrapy.Request(url=next_page, callback=self.parse2)
        products = response.xpath(".//div[@class='productLists']//li")
        if len(products) > 0:
            category = response.xpath("//div[@class='headerContent']/h1/text()").extract_first().strip()
            for item in products:
                product = TescoItem()
                if item.xpath(".//h3[@class='inBasketInfoContainer']/a/@href").extract_first() is not None:
                    url = item.xpath(".//h3[@class='inBasketInfoContainer']/a/@href").extract_first()
                    id = item.xpath(".//h3[@class='inBasketInfoContainer']/a/@id").extract_first().split('-')[1]
                    product['id'] = id
                    product['url'] = 'https://www.tesco.ie' + url
                    product['img'] = item.xpath(".//span[@class='image']/img/@src").extract_first()
                    product['name'] = item.xpath(".//h3[@class='inBasketInfoContainer']/a/text()").extract_first().strip()
                    product['category'] = category
                    product['price'] = float(
                        item.xpath(".//span[@class='linePrice']/text()").extract_first().replace(u'€', '').replace(
                            ',', ''))
                    yield product