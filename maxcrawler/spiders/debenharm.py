import scrapy

from maxcrawler.items import DebenhamsItem

class DebenharmSpider(scrapy.Spider):
    name = "debenharm"

    def start_requests(self):
        url = 'https://www.debenhams.ie/'
        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        urls = response.xpath(".//div[@class='t-desktop-nav__wrapper']//a/@href").extract()
        print(urls)
        for url in urls:
            if '/sale' not in url:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse1)

    def parse1(self, response):
        urls = response.xpath(".//div[contains(@class, 'pw-accordion__item')][1]//a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin('/mobify/proxy/base' + url), callback=self.parse2)

    def parse2(self, response):
        pag = response.xpath(".//div[@id='pagination']//a/text()").extract()
        if len(pag) is 0:
            yield scrapy.Request(url=response.url, callback=self.parser3)
        else:
            count = int(pag[len(pag)-2])
            for i in range(1,count+1):
                yield scrapy.Request(url=response.url+'?pn='+str(i), callback=self.parser3)

    def parser3(self, response):
        items = response.xpath("//div[contains(@class, 'item_container')]")
        category = response.xpath("//span[@class='breadcrumb current']/text()").extract_first().strip()
        for item in items:
            product = DebenhamsItem()
            url = item.xpath(".//div[@class='description']/a[1]/@href").extract_first()
            url_parts = url.split('/')
            last_part = url_parts[-1]
            part_of_codes = last_part.split('_')
            code = part_of_codes[-2]
            code = code.replace('+','_')
            product['id'] = code
            product['url'] = 'https://www.debenhams.ie' + item.xpath(".//div[@class='description']/a[1]/@href").extract_first()
            product['img'] = item.xpath(".//img[@itemprop='image']/@src").extract_first()
            product['brand'] = item.xpath(".//div[@itemprop='brand']/text()").extract_first().strip()
            product['name'] = item.xpath(".//div[@itemprop='name']/text()").extract_first().strip()
            product['category'] = category
            product['price'] = float(item.xpath(".//span[@itemprop='price']/text()").extract_first().split('-')[0].replace(u'€', '').replace(',','').replace('Now',''))
            yield product

